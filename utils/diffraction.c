#include <math.h>
#include <stdlib.h>

#define NINT(x) ((int)((x)>0.0?(x)+0.5:(x)-0.5))

/**
*  insert diffractor in the model used, in makemod
*
*   AUTHOR:
*           Jan Thorbecke (janth@xs4all.nl)
*           The Netherlands 
**/


void diffraction(float *x, float *z, int nxp, float dx, float dz, float **gridcp, float **gridcs, float **gridro, float **cp, float **cs, float **ro, float *interface, int *zp, int nx)
{
	int     i;

	for (i = 0; i < nx; i++) {
		interface[i] = 0.0;
		zp[i] = 0;
	}

	if (gridcs == NULL && gridro == NULL) {
		for (i = 0; i < nxp; i++) {
			interface[NINT(x[i]/dx)] = z[i];
			zp[NINT(x[i]/dx)] = NINT(z[i]/dz);
			gridcp[NINT(x[i]/dx)][NINT(z[i]/dz)] = cp[2][i];
		}
	}
	else if (gridcs == NULL) {
		for (i = 0; i < nxp; i++) {
			interface[NINT(x[i]/dx)] = z[i];
			zp[NINT(x[i]/dx)] = NINT(z[i]/dz);
			gridcp[NINT(x[i]/dx)][NINT(z[i]/dz)] = cp[2][i];
			gridro[NINT(x[i]/dx)][NINT(z[i]/dz)] = ro[2][i];
		}
	}
	else {
		for (i = 0; i < nxp; i++) {
			interface[NINT(x[i]/dx)] = z[i];
			zp[NINT(x[i]/dx)] = NINT(z[i]/dz);
			gridcp[NINT(x[i]/dx)][NINT(z[i]/dz)] = cp[2][i];
			gridcs[NINT(x[i]/dx)][NINT(z[i]/dz)] = cs[2][i];
			gridro[NINT(x[i]/dx)][NINT(z[i]/dz)] = ro[2][i];
		}
	}

	return;
}
