/*
 *  decomposition.h
 *  
 *
 *  Created by Jan Thorbecke on 17/03/2011.
 *  Copyright 2011 TU Delft. All rights reserved.
 *
 */

void kxwfilter(complex *data, float k, float dx, int nkx, 
			   float alfa1, float alfa2, float perc);

void kxwdecomp(complex **recx, complex **recz, complex **p, complex **s,
               int nsam, float dx, int nrec, float df, float fmin, float fmax,
               float cp, float cs, float alfa1, float alfa2, float rho,
               float perc, int choice, int meas)
{
	int      iom, iomin, iomax, i, err;
	float    omin, omax, deltom, om, kp, ks;
	complex  *dpux, *dpuz, *dsux, *dsuz;
	complex  *ax, *az;
	
	deltom = 2.*PI*df;
	omin   = 2.*PI*fmin;
	omax   = 2.*PI*fmax;
	
	iomin  = (int)MIN((omin/deltom), (nrec-1));
	iomin  = MAX(iomin, 1);
	iomax  = MIN((int)(omax/deltom), (nrec-1));

	dpuz   = alloc1complex(nsam);
	dpux   = alloc1complex(nsam);
	dsuz   = alloc1complex(nsam);
	dsux   = alloc1complex(nsam);
	az     = alloc1complex(nsam);
	ax     = alloc1complex(nsam);


	decps(om, rho, cp, cs, dx, nsam, 
		  dpux, dpuz, dsux, dsuz, meas);
	kxwfilter(dpux, kp, dx, nsam, alfa1, alfa2, perc); 
	kxwfilter(dpuz, kp, dx, nsam, alfa1, alfa2, perc); 
	kxwfilter(dsux, ks, dx, nsam, alfa1, alfa2, perc); 
	kxwfilter(dsuz, ks, dx, nsam, alfa1, alfa2, perc); 

	for (i = 0; i < nsam; i++) {
		ax[i] = cmul(dpux[i], recx[iom][i]);
		az[i] = cmul(dpuz[i], recz[iom][i]);
		p[iom][i] = cadd(ax[i], az[i]);
		ax[i] = cmul(dsux[i], recx[iom][i]);
		az[i] = cmul(dsuz[i], recz[iom][i]);
		s[iom][i] = cadd(ax[i], az[i]);
	}
	free1complex(dpuz);
	free1complex(dpux);
	free1complex(dsuz);
	free1complex(dsux);
	free1complex(az);
	free1complex(ax);
	return;
}

void decps(float om, float rho, float cp, float cs, float dx, int nkx, complex *pux, complex *puz, complex *sux, complex *suz, int meas)
{
	int 	 ikx;
	float 	 mu, kp, kp2, ks, ks2, ksk;
	float 	 kx, kx2, kzp2, kzs2, dkx;
	complex kzp, kzs, cste;
	
	mu = rho*cs*cs;
	if (meas == 1 ) {
		cste.r   = 0.0;
		cste.i   = mu;
	}
	
	else if (meas == 2) {
		cste.r = mu/om;
		cste.i = 0.0;
	}
	kp  = om/cp;
	kp2 = kp*kp;
	ks  = om/cs;
	ks2 = ks*ks;
	dkx = 2.0*PI/(nkx*dx);
	
	for (ikx = 0; ikx <= (nkx/2); ikx++) {
		kx   = ikx*dkx;
		kx2  = kx*kx;
		kzp2 = kp2 - kx2;
		kzs2 = ks2 - kx2;
		ksk  = ks2 - 2.0*kx2;
		kzp  = froot(kzp2);
		kzs  = froot(kzs2);
		
		pux[ikx] = crmul(cste, kx);
		suz[ikx] = crmul(cste, kx);
		
		if (kzp2 != 0) 
			puz[ikx] = crmul(cdiv(cste, crmul(kzp, 2.0)),-ksk);
		else 
			puz[ikx] = cmplx(0.0, 0.0);
		if (kzs2 != 0)
			sux[ikx] = crmul(cdiv(cste, crmul(kzs, 2.0)), ksk);
		else 
			sux[ikx] = cmplx(0.0, 0.0);
	}
	
	for (ikx = (nkx/2+1); ikx < nkx; ikx++) {
		pux[ikx] = crmul(pux[nkx-ikx], -1.0);
		puz[ikx] = puz[nkx-ikx];
		sux[ikx] = sux[nkx-ikx];
		suz[ikx] = crmul(suz[nkx-ikx], -1.0);
	}
	
	return;
}
